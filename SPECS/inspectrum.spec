Name:           inspectrum
Version:        0.2.3
Release:        1%{?dist}
Summary:        Offline radio signal analyzer

License:        GPLv3
URL:            https://github.com/miek/inspectrum
Source0:        https://github.com/miek/inspectrum/archive/v%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  gcc
BuildRequires:  qt5-qtbase-devel
BuildRequires:  pkg-config
BuildRequires:  liquid-dsp-devel
BuildRequires:  fftw-devel
Requires:       qt5-qtbase
Requires:       liquid-dsp
Requires:       fftw

%description
Inspectrum is a tool for analyzing captured signals, primarily from
software-defined radio receivers.

%global debug_package %{nil}
%prep
%autosetup
mkdir build
cd build
cmake ..


%build
cd build
%make_build


%install
cd build
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license LICENSE
%doc README.md
/usr/local/bin/inspectrum


%changelog
* Thu Jan 21 03:32:04 CET 2021 Dominik Rimpf <dev@drimpf.de> - 0.2.3-1
- New release 0.2.3

* Mon Jul 27 2020 Dominik Rimpf <dev@d-rimpf.de> - 0.2.2-2.20200707git60a6dce
- Updated to newest commit

* Wed Jan 15 2020 Dominik Rimpf <dev@d-rimpf.de> 0.2.2.20191228gitd6115cb4
- Upgraded to snapshot version because of multiple bugfixes
* Tue Oct 29 2019 Dominik Rimpf <dev@d-rimpf.de>
- First rpm build
